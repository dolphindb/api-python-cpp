#!/bin/bash
rm -rf build/*
rm -rf dist/*
rm -rf wheel/*

# Python36 setup.py bdist_wheel
# Python37 setup.py bdist_wheel
# export MACOSX_DEPLOYMENT_TARGET=10.16
/Library/Frameworks/Python.framework/Versions/3.8/bin/python3.8 setup.py bdist_wheel

# mv dist/dolphindb-1.30.19.1-cp38-cp38-macosx_12_0_x86_64.whl wheel/dolphindb-1.30.19.1-cp38-cp38-macosx_10_16_x86_64.whl
# Python39 setup.py bdist_wheel

# mkdir -p wheelhouse

# # PLAT=macosx_10_16

# for whl in dist/*.whl; do
#     echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ${PYBIN}"
#     delocate-wheel -w wheels_fixed -v "$whl"
# done