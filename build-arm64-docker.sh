#!/bin/bash
rm -rf build/*
rm -rf wheel/*
docker run --rm -e PLAT=manylinux2014_aarch64 -v /usr/bin/qemu-aarch64-static:/usr/bin/qemu-aarch64-static -v $RPATH/api-python-cpp:/io dev-arm:v1 /io/build-wheels.sh